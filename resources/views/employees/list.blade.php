@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Welcome</div>

                <div class="panel-body">
                    {!! Form::open(['url' => url('/employees'), 
                    'method' => 'get', 'class'=>'form-inline']) !!}
                        @include('employees._search')
                    {!! Form::close() !!}

                    <div id="result"></div>

                </div>
            </div>
        </div>
    </div>
</div>
<script>
    var obj = {!! $result !!}
    $("#result").jJsonViewer(obj,{expanded: true});
</script>
@endsection

